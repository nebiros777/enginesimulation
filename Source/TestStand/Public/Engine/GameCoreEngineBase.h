// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Data/GameCoreData.h"
#include "UObject/NoExportTypes.h"
#include "GameCoreEngineBase.generated.h"

DECLARE_MULTICAST_DELEGATE_TwoParams(FloatIntParamDelegate, const float, const int32)
DECLARE_MULTICAST_DELEGATE_OneParam(FIntParamDelegate, const int32)

UCLASS()
class TESTSTAND_API AGameCoreEngineBase : public AActor
{
	GENERATED_BODY()

public:
	AGameCoreEngineBase();

	virtual void StartEngine();
	virtual int32 GetElapsedTime();
	virtual float GetEngineTemperature();
	virtual void SetEngineType(TSharedPtr<FEngineTypesDataRow> InEngineType);
	virtual void SetEnvironmentTemperature(const float InEnvironmentTemperature);
	virtual void SetSimulatedSecondTime(const float InSimulatedSecondTime);
	virtual void StopEngine();

private:
	virtual void Simulate();

	UFUNCTION()
	void SimulateSecond();

	void SetTemperature(float NewTemperature);
	void CalculateEngineTemperature();
	void CheckOverheating();
	float GetCoolingSpeed();
	float GetHeatingSpeed();

	TSharedPtr<FEngineTypesDataRow> EngineType;

	FTimerHandle SimulationTimerHandle;

	float CurrentCoolantTemperature{0.0f}; // Celsius
	float CurrentTorque{0.0f}; // Moment
	float CurrentRPM{0.0f}; // Angular velocity
	float CurrentAcceleration{0.0f}; // Angular acceleration
	int8 Second{1}; // Virtual second

	float SimulatedSecontTime{0.1f}; // = 1 Second in real time
	float EnvironmentTemperature{20.f};

	int32 ElapsedTime{0};

public:
	FloatIntParamDelegate OnEngineTemperatureChanged;
	FIntParamDelegate OnEngineOverheated;
};
