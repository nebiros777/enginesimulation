// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameCoreMainMenu.generated.h"

class UComboBoxString;
class UEditableText;
class UTextBlock;
class UButton;

DECLARE_MULTICAST_DELEGATE_TwoParams(FOnStartTestClicked, float, float);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnEngineSelected,const FString&);

UCLASS()
class TESTSTAND_API UGameCoreMainMenu : public UUserWidget
{
	GENERATED_BODY()
	
	virtual void NativeConstruct() override;

	FText ClearText(const FText& Input);
	float ConvertToFloat(const FText& Input);

public:
	void SetEngineTemperature(float CurrentTemperature);
	void SetEngines(const TMap<FString, FString> InEngines);

	UFUNCTION()
	void OnEnvironmentTemperatureTextChanged(const FText& Text);
	UFUNCTION()
	void OnSimulatedSecondTextChanged(const FText& Text);
	UFUNCTION()
	void OnStartTestButtonClicked();
	UFUNCTION(BlueprintCallable)
	void OnEngineSelectedAgain(const FString& SelectedItem);
	void SetElapsedTime(int32 InElapsedTime);

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UTextBlock* CurrentEngineTemperatureText;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UTextBlock*  ElapsedTimeText;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UButton* StartTestButton;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UEditableText* EnvironmenTemperatureText;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UEditableText* SimulatedSecondText;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UComboBoxString* ComboBox;

	TMap<FString, FString> Engines;

public:

	FOnEngineSelected OnEngineSelected;

	FOnStartTestClicked OnStartTestClicked;
	
};
