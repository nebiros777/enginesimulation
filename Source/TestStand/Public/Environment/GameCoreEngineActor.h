// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameCoreEngineBase.h"
#include "GameCoreEngineActor.generated.h"

UCLASS()
class TESTSTAND_API AGameCoreEngineActor : public AGameCoreEngineBase
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	AGameCoreEngineActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SetMaterialTemperature(float Temperature);

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* StaticMesh;
};
