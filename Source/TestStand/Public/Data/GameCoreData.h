#pragma once

#include "Engine/DataTable.h"
#include "GameCoreData.generated.h"

USTRUCT(BlueprintType)
struct FEngineTypesDataRow : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString EngineType;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float  InertiaMoment; // I = Inertia moment in kg*m^2

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float  OverheatingTemperature; // T = Overheating temperature in Celsius

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float  HeatingRateOnTorqueMultiplier; // Hm = Heating rate on torque multiplier in Celsius/(N*m)

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float  HeatingRateOnRPMMultiplier; // Hv = Heating rate on RPM multiplier in Celsius/(rad/s)

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float  CoolingRateMultiplier; // C = Cooling rate multiplier in Celsius/s

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UCurveFloat* TorqueOnRPMCurveFloat; // Torque on RPM curve function

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UStaticMesh* EngineMesh; // Engine mesh
};