// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "GameCorePlayerController.generated.h"

class UDataTable;
/**
 * 
 */
UCLASS()
class TESTSTAND_API AGameCorePlayerController : public APlayerController
{
	GENERATED_BODY()

	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly)
	TSoftObjectPtr<UDataTable> EngineTypesDataTable{nullptr};

public:
	TSoftObjectPtr<UDataTable> GetEngineTypesDataTable() const { return EngineTypesDataTable; }
};
