// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "GameCoreHUD.generated.h"

class UGameCoreMainMenu;
/**
 * 
 */
UCLASS()
class TESTSTAND_API AGameCoreHUD : public AHUD
{
	GENERATED_BODY()

	virtual void BeginPlay() override;
	void OnStartTestClicked(float EnvironmenTemperature, float SimulatedSecond);
	void OnEngineSelected(const FString& EngineId);

	UPROPERTY(EditDefaultsOnly)
	TSoftClassPtr<UGameCoreMainMenu> MainMenuWidgetClass;

	UPROPERTY()
	TWeakObjectPtr<UGameCoreMainMenu> MainMenuWidget;


public:
	void SetEngineTemperatureAndTime(float CurrentTemperature, const int32 ElapsedTime);

	UFUNCTION(BlueprintImplementableEvent)
	void ShowLoadingScreen();
	UFUNCTION(BlueprintImplementableEvent)
	void RemoveLoadingScreen();
};
