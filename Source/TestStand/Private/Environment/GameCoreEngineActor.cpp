// Fill out your copyright notice in the Description page of Project Settings.


#include "Environment/GameCoreEngineActor.h"

// Sets default values
AGameCoreEngineActor::AGameCoreEngineActor()
{
	PrimaryActorTick.bCanEverTick = true;
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	RootComponent = StaticMesh;
}

// Called when the game starts or when spawned
void AGameCoreEngineActor::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AGameCoreEngineActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGameCoreEngineActor::SetMaterialTemperature(float Temperature)
{
	FName ParameterName("Temperature");

	if (UMaterialInstanceDynamic* MeshCompMaterial = StaticMesh->CreateAndSetMaterialInstanceDynamic(0))
	{
		MeshCompMaterial->SetScalarParameterValue(ParameterName, Temperature);
	}
}

