// Fill out your copyright notice in the Description page of Project Settings.


#include "GameCorePlayerController.h"

#include "Environment/GameCoreEngineActor.h"
#include "Kismet/GameplayStatics.h"

void AGameCorePlayerController::BeginPlay()
{
	Super::BeginPlay();

	FInputModeUIOnly InputMode;	
	SetInputMode(InputMode);
	SetShowMouseCursor(true);

	FTimerHandle StartTimerHandle;
	GetWorld()->GetTimerManager().SetTimer(StartTimerHandle, FTimerDelegate::CreateWeakLambda(this, [this]()
	{
		auto Camera = UGameplayStatics::GetActorOfClass(GetWorld(), AGameCoreEngineActor::StaticClass());
		if (Camera)
		{
			SetViewTarget(Camera);
		}
	}), 1.0f, false);
}
