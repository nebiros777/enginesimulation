// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GameCoreGameMode.generated.h"

/**
 * 
 */
UCLASS()
class TESTSTAND_API AGameCoreGameMode : public AGameModeBase
{
	GENERATED_BODY()

};
