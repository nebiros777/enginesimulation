// Fill out your copyright notice in the Description page of Project Settings.


#include "Engine/GameCoreEngineBase.h"

AGameCoreEngineBase::AGameCoreEngineBase()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AGameCoreEngineBase::StartEngine()
{
	ElapsedTime = 0;
	CurrentTorque = 0.0f;
	CurrentRPM = 0.0f;
	CurrentAcceleration = 0.0f;

	SetTemperature(EnvironmentTemperature);

	Simulate();
}

void AGameCoreEngineBase::Simulate()
{
	UWorld* World = GetWorld();

	if (!World)
	{
		return;
	}

	if (World->GetTimerManager().IsTimerActive(SimulationTimerHandle))
	{
		World->GetTimerManager().ClearTimer(SimulationTimerHandle);
	}

	GetWorld()->GetTimerManager().SetTimer(SimulationTimerHandle, this, &AGameCoreEngineBase::SimulateSecond, SimulatedSecontTime, true);
}

void AGameCoreEngineBase::CalculateEngineTemperature()
{
	CurrentTorque = EngineType->TorqueOnRPMCurveFloat->GetFloatValue(CurrentRPM);
	CurrentAcceleration = CurrentTorque / EngineType->InertiaMoment;
	CurrentRPM += CurrentAcceleration * Second;

	CurrentCoolantTemperature += (GetHeatingSpeed() - GetCoolingSpeed()) * Second;

	OnEngineTemperatureChanged.Broadcast(CurrentCoolantTemperature, ElapsedTime);
}

void AGameCoreEngineBase::CheckOverheating()
{
	if (CurrentCoolantTemperature >= EngineType->OverheatingTemperature)
	{
		OnEngineOverheated.Broadcast(ElapsedTime);
	}
}

float AGameCoreEngineBase::GetCoolingSpeed()
{
	return EngineType->CoolingRateMultiplier * (EnvironmentTemperature - CurrentCoolantTemperature);
}

float AGameCoreEngineBase::GetHeatingSpeed()
{
	return CurrentTorque * EngineType->HeatingRateOnTorqueMultiplier + FMath::Square(CurrentRPM) * EngineType->HeatingRateOnRPMMultiplier;
}

int32 AGameCoreEngineBase::GetElapsedTime()
{
	return ElapsedTime;
}

float AGameCoreEngineBase::GetEngineTemperature()
{
	return CurrentCoolantTemperature;
}

void AGameCoreEngineBase::SetEngineType(TSharedPtr<FEngineTypesDataRow> InEngineType)
{
	EngineType = InEngineType;
}

void AGameCoreEngineBase::SetEnvironmentTemperature(const float InEnvironmentTemperature)
{
	EnvironmentTemperature = InEnvironmentTemperature;
}

void AGameCoreEngineBase::SetSimulatedSecondTime(const float InSimulatedSecondTime)
{
	SimulatedSecontTime = InSimulatedSecondTime;
}

void AGameCoreEngineBase::StopEngine()
{
	if (GetWorld()->GetTimerManager().IsTimerActive(SimulationTimerHandle))
	{
		GetWorld()->GetTimerManager().ClearTimer(SimulationTimerHandle);
	}
}

void AGameCoreEngineBase::SimulateSecond()
{
	ElapsedTime++;
	CalculateEngineTemperature();
	CheckOverheating();
}

void AGameCoreEngineBase::SetTemperature(float NewTemperature)
{
	CurrentCoolantTemperature = NewTemperature;
}
