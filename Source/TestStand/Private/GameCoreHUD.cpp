// Fill out your copyright notice in the Description page of Project Settings.


#include "GameCoreHUD.h"
#include "GameCoreStandController.h"
#include "Blueprint/UserWidget.h"
#include "UserInterface/GameCoreMainMenu.h"

void AGameCoreHUD::OnStartTestClicked(float EnvironmenTemperature, float SimulatedSecond)
{
	STAND_CONTROLLER()->StartTest(EnvironmenTemperature, SimulatedSecond);
}

void AGameCoreHUD::OnEngineSelected(const FString& EngineId)
{
	STAND_CONTROLLER()->LoadEngine(EngineId);
}

void AGameCoreHUD::BeginPlay()
{
	Super::BeginPlay();
	ShowLoadingScreen();
	FTimerHandle StartTimerHandle, RemoveLoadingScreenTimerHandle;
	GetWorld()->GetTimerManager().SetTimer(StartTimerHandle, FTimerDelegate::CreateWeakLambda(this, [this]()
	{
		UUserWidget* UW = CreateWidget<UUserWidget>(GetGameInstance(), MainMenuWidgetClass.LoadSynchronous());
		if(UW)
		{
			MainMenuWidget = Cast<UGameCoreMainMenu>(UW);
			if (!MainMenuWidget.IsValid())
			{
				UE_LOG(LogTemp, Error, TEXT("MainMenuWidget is not valid"));
				return;
			}			
			MainMenuWidget->AddToViewport(0);
			MainMenuWidget->SetEngines(STAND_CONTROLLER()->GetTmapEngines());
			MainMenuWidget->OnStartTestClicked.AddUObject(this, &AGameCoreHUD::OnStartTestClicked);
			MainMenuWidget->OnEngineSelected.AddUObject(this, &AGameCoreHUD::OnEngineSelected);
		}
	}), 1.0f, false);

	GetWorld()->GetTimerManager().SetTimer(RemoveLoadingScreenTimerHandle, FTimerDelegate::CreateWeakLambda(this, [this]()
	{
		RemoveLoadingScreen();
	}), 2.f, false);

}

void AGameCoreHUD::SetEngineTemperatureAndTime(float CurrentTemperature, const int32 ElapsedTime)
{
	if (MainMenuWidget.IsValid())
	{
		MainMenuWidget->SetEngineTemperature(CurrentTemperature);
		MainMenuWidget->SetElapsedTime(ElapsedTime);
	}
}
