// Copyright Epic Games, Inc. All Rights Reserved.

#include "TestStand.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TestStand, "TestStand" );
