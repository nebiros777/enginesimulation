// Fill out your copyright notice in the Description page of Project Settings.


#include "GameCoreStandController.h"

#include "GameCoreHUD.h"
#include "GameCorePlayerController.h"
#include "Data/GameCoreData.h"
#include "Engine/DataTable.h"
#include "Engine/GameCoreEngineBase.h"
#include "Environment/GameCoreEngineActor.h"
#include "Kismet/GameplayStatics.h"

UGameCoreStandController::UGameCoreStandController() : Super()
{
	const FString EngineTypesDataTablePath = TEXT("/Script/Engine.DataTable'/Game/DataTables/DT_EngineTypes.DT_EngineTypes'"); //"DataTable'/GameCore/Assets/ErrorHandling/ErrorHandlingDataTable.ErrorHandlingDataTable'");
	EngineTypesDataTable = TSoftObjectPtr<UDataTable>(FSoftObjectPath(EngineTypesDataTablePath));
}

void UGameCoreStandController::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);

	AGameCorePlayerController* LocalPC = Cast<AGameCorePlayerController>(GEngine->GetFirstLocalPlayerController(GetWorld()));
	if (LocalPC)
	{
		EngineTypesDataTable = LocalPC->GetEngineTypesDataTable();
	}
}

void UGameCoreStandController::StartTest(const float EnvironmenTemperature,const float SimulatedSecond)
{
	if (GameCoreEngineActorPtr)
	{
		GameCoreEngineActorPtr->SetMaterialTemperature(EnvironmenTemperature);
	}
	if (Engine)
	{
		Engine->SetEnvironmentTemperature(EnvironmenTemperature);
		Engine->SetSimulatedSecondTime(SimulatedSecond);
		Engine->StartEngine();
	}
}

TMap<FString, FString> UGameCoreStandController::GetTmapEngines()
{
	TMap<FString, FString> Engines;
	if (EngineTypesDataTable.IsPending() && !EngineTypesDataTable.LoadSynchronous())
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to retrieve database"));
		return Engines;
	}

	TArray<FName> RowNames = EngineTypesDataTable->GetRowNames();
	for (auto It = RowNames.CreateConstIterator(); It; ++It)
	{
		FString RowName = It->ToString();
		FEngineTypesDataRow* Row = EngineTypesDataTable->FindRow<FEngineTypesDataRow>(*RowName, TEXT(""));
		Engines.Add(RowName, Row->EngineType);
	}
	return Engines;
}

void UGameCoreStandController::StopTest(const int32 ElapsedTime)
{
	if (Engine)
	{
		Engine->StopEngine();
	}
}

void UGameCoreStandController::LoadEngine(const FString& EngineId)
{
	if (EngineTypesDataTable.IsPending() && !EngineTypesDataTable.LoadSynchronous())
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to retrieve database"));
		return;
	}

	auto EngineActor = UGameplayStatics::GetActorOfClass(GetWorld(), AGameCoreEngineActor::StaticClass());
	if (EngineActor) 
	{
		GameCoreEngineActorPtr = Cast<AGameCoreEngineActor>(EngineActor);
	}

	FEngineTypesDataRow* TableRowInfo = EngineTypesDataTable->FindRow<FEngineTypesDataRow>(*EngineId, TEXT(""));

	if (TableRowInfo)
	{
		Engine = Cast<AGameCoreEngineBase>(GameCoreEngineActorPtr);
		Engine->SetEngineType(MakeShared<FEngineTypesDataRow>(*TableRowInfo));
		Engine->OnEngineOverheated.AddUObject(this, &UGameCoreStandController::StopTest);
		Engine->OnEngineTemperatureChanged.AddUObject(this, &UGameCoreStandController::UpdateEngineAndHud);
		CreateEngineActor(TableRowInfo->EngineMesh);
	}
}

void UGameCoreStandController::UpdateEngineAndHud(const float CurrentTemperature, const int32 ElapsedTime)
{
	APlayerController* LocalPC = GEngine->GetFirstLocalPlayerController(GetWorld());
	if (LocalPC && LocalPC->GetHUD())
	{
		TWeakObjectPtr<AGameCoreHUD> HUDptr = LocalPC->GetHUD<AGameCoreHUD>();
		if (HUDptr.IsValid())
		{
			HUDptr->SetEngineTemperatureAndTime(CurrentTemperature, ElapsedTime);
		}
	}
	if (GameCoreEngineActorPtr)
	{
		GameCoreEngineActorPtr->SetMaterialTemperature(CurrentTemperature);
	}
}

void UGameCoreStandController::CreateEngineActor(UStaticMesh* StaticMesh)
{
	if (GameCoreEngineActorPtr)
	{
		GameCoreEngineActorPtr->StaticMesh->SetStaticMesh(StaticMesh);
		GameCoreEngineActorPtr->SetMaterialTemperature(0);
	}

}
