// Fill out your copyright notice in the Description page of Project Settings.


#include "UserInterface/GameCoreMainMenu.h"
#include "Components/Button.h"
#include "Components/ComboBoxString.h"
#include "Components/EditableText.h"
#include "Components/TextBlock.h"
#include "Misc/DefaultValueHelper.h"

void UGameCoreMainMenu::NativeConstruct()
{
	Super::NativeConstruct();

	EnvironmenTemperatureText->OnTextChanged.AddUniqueDynamic(this, &UGameCoreMainMenu::OnEnvironmentTemperatureTextChanged);
	SimulatedSecondText->OnTextChanged.AddUniqueDynamic(this, &UGameCoreMainMenu::OnSimulatedSecondTextChanged);
	StartTestButton->OnClicked.AddUniqueDynamic(this, &UGameCoreMainMenu::OnStartTestButtonClicked);
}

void UGameCoreMainMenu::OnEnvironmentTemperatureTextChanged(const FText& Text)
{
	EnvironmenTemperatureText->SetText(ClearText(Text));
}

void UGameCoreMainMenu::OnSimulatedSecondTextChanged(const FText& Text)
{
	SimulatedSecondText->SetText(ClearText(Text));
}

void UGameCoreMainMenu::OnStartTestButtonClicked()
{
	OnStartTestClicked.Broadcast(ConvertToFloat(EnvironmenTemperatureText->GetText()), ConvertToFloat(SimulatedSecondText->GetText()));
}

void UGameCoreMainMenu::OnEngineSelectedAgain(const FString& SelectedItem)
{
	OnEngineSelected.Broadcast(*Engines.FindKey(SelectedItem));
}

void UGameCoreMainMenu::SetElapsedTime(int32 InElapsedTime)
{
	ElapsedTimeText->SetText(FText::FromString(FString::FromInt(InElapsedTime)));
}

FText UGameCoreMainMenu::ClearText(const FText& Input)
{
	FString Temp = Input.ToString();
	int32 NumberOfDots = 1;
	for (int32 i = 0; i < Temp.Len(); ++i)
	{
		if (!FChar::IsDigit(Temp[i]))
		{
			if (Temp[i] == TCHAR('.') && NumberOfDots > 0)
			{
				NumberOfDots--;
			}
			else
			{
				Temp.RemoveAt(i);
				--i;
			}
		}
	}
	return FText::FromString(Temp);
}

float UGameCoreMainMenu::ConvertToFloat(const FText& Input)
{
	FString SanitizedString = Input.ToString();;
	float Result;
	FDefaultValueHelper::ParseFloat(SanitizedString, Result);
	return Result;
}

void UGameCoreMainMenu::SetEngineTemperature(float CurrentTemperature)
{
	CurrentEngineTemperatureText->SetText(FText::FromString(FString::SanitizeFloat(CurrentTemperature)));
}

void UGameCoreMainMenu::SetEngines(const TMap<FString, FString> InEngines)
{
	Engines = InEngines;

	for (auto& Engine : InEngines)
	{
		FString EngineName = Engine.Value;
		ComboBox->AddOption(EngineName);
	}
}

