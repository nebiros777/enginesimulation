// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Data/GameCoreData.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "GameCoreStandController.generated.h"

#define STAND_CONTROLLER() GetWorld()->GetGameInstance()->GetSubsystem<UGameCoreStandController>()
#define STAND_CONTROLLER_WORLD(World) World->GetGameInstance()->GetSubsystem<UGameCoreStandController>()

class AGameCoreEngineActor;
class AGameCoreEngineBase;
class UDataTable;

UCLASS()
class TESTSTAND_API UGameCoreStandController : public UGameInstanceSubsystem
{
	GENERATED_BODY()

	UGameCoreStandController();
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;

public:
	void StartTest(const float EnvironmenTemperature,const float SimulatedSecond);
	void LoadEngine(const FString& EngineId);
	TMap<FString, FString> GetTmapEngines();

private:
	UFUNCTION()
	void StopTest(const int32 ElapsedTime);
	void UpdateEngineAndHud(const float CurrentTemperature, const int32 ElapsedTime);
	void CreateEngineActor(UStaticMesh* StaticMesh);

	UPROPERTY()
	TSoftObjectPtr<UDataTable> EngineTypesDataTable{nullptr};

	UPROPERTY()
	AGameCoreEngineBase* Engine{nullptr};

	UPROPERTY()
	AGameCoreEngineActor* GameCoreEngineActorPtr{nullptr};

};
